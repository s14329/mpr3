/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import data.Order;
import java.util.List;

/**
 *
 * @author shymek
 */
public interface OrderDao {
    
    public void clearOrder();
    public List<Order> getAllOrders();
    public int addOrder (Order order);
    
}
