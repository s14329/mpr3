/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import data.OrderItem;
import java.util.List;

/**
 *
 * @author shymek
 */
public interface OrderItemDao {
    
    public void clearOrderItem();
    public List<OrderItem> getAllOrderItems();
    public int addOrderItem (OrderItem orderItem);
    
}
