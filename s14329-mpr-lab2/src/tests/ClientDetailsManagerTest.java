/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import data.ClientDetails;
import logic.ClientDetailsManager;
import static org.junit.Assert.*;
import java.util.List;

/**
 *
 * @author shymek
 */
public class ClientDetailsManagerTest {
    
    /*
    private String name;
    private String surname;
    private String login;
    */
    
    ClientDetailsManager clientDetailsManager = new ClientDetailsManager() {
        @Override
        public List<ClientDetails> getAllClientDetails() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    };
    
    private final static String NAME_1 = "Szymon";
    private final static String SURNAME_1 = "Polak";
    private final static String LOGIN_1 = "szymonpolak";
    
    public void checkConnection(){
        assertNotNull(clientDetailsManager.getConnection());
    }
    
    public void checkAdding(){
        
        ClientDetails clientDetails = new ClientDetails(NAME_1, SURNAME_1, LOGIN_1);
        
        clientDetailsManager.clearClientDetails();
        assertEquals(1,clientDetailsManager.addClientDetails(clientDetails));
        
        List<ClientDetails> clientsDetails = clientDetailsManager.getAllClientsDetails();
        ClientDetails clientDetailsRetrieved = clientsDetails.get(0);
        
        assertEquals(NAME_1, clientDetailsRetrieved.getName());
        assertEquals(SURNAME_1, clientDetailsRetrieved.getSurname());
        assertEquals(LOGIN_1, clientDetailsRetrieved.getLogin());
        
    }
    
}
